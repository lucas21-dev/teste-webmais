const { create, getComments } = require('../models/CommentsModel');
const { getAll } = require('../models/ProductsModel');

const createService = async ({ code, comment, productId }) => {
  const products = await getAll();

  const verifyProduct = products.filter((product) => product.code === code);
  if(!verifyProduct.length) return null;

  const commentId = await create({ comment, productId });

  return {
    id: commentId,
    productId,
    comment,
  };
}

const getService = async () => {
  const products = await getAll();

  const productsToDisplay = products.map((product) => ({
    productId: product.id,
    name: product.name
  }));

  const comments = await getComments();

  const orderedComments = productsToDisplay.map((id) => ({
    ...id,
    comments: comments.filter((com) => com['product_id'] === id.productId)
      .map((com) => com.comment),
  }));

  const editedComments =  orderedComments.map(({ productId, name, comments }) => ({
    productId,
    name,
    comments: comments.length ? comments : 'Ainda não possui comentários',
  }));

  return editedComments;
}

module.exports = {
  createService,
  getService,
}