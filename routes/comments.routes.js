const express = require('express');

const { validateComment } = require('../middlewares')
const { createComment, getComments } = require('../controllers/commentController');

const commentRouter = express.Router();

commentRouter.post('/', validateComment, createComment);

commentRouter.get('/', getComments);

module.exports = commentRouter;