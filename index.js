const express = require('express');
require('dotenv').config();
require('express-async-errors');

const { errorHandler } = require('./middlewares');
const { productRouter, commentRouter } = require('./routes/index');

const app = express();
app.use(express.json());

app.use('/products', productRouter);

app.use('/comments', commentRouter);

app.use(errorHandler);

const PORT = process.env.PORT || 3000;

app.listen(PORT, () => {
  console.log(`Rodando na porta ${PORT}`);
});
