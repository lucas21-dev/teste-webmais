const commentCreate = require('../schemas/commentCreate');

const validateComment = async (req, res, next) => {
  const { error } = commentCreate.validate(req.body);

  if (error) {
    const [code, message] = error.message.split('|');
    return res.status(Number(code)).json({ message });
  }

  next();
};

module.exports = validateComment;