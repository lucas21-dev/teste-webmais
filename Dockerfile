FROM node:16-alpine

# ENV HOST=db
# ENV MYSQL_USER=root
# ENV MYSQL_PASSWORD=password
# ENV MYSQL_PORT=3306

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

EXPOSE 3000
