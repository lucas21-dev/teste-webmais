const { getAll } = require('../models/ProductsModel');
const { createService } = require('../services/productsService');

const getProducts = async (_req, res) => {
  const products = await getAll();

  return res.status(200).json(products);
};

const createProduct = async (req, res) => {
  const { code, name, height, width, depth  } = req.body;
  const createdProduct = await createService({ code, name, height, width, depth });

  if(!createdProduct) return res.status(409).json({ message: 'O código de produto já está cadastrado!' });

  return res.status(201).json(createdProduct);
}

module.exports = {
  getProducts,
  createProduct,
};