const express = require('express');

const { validateProduct } = require('../middlewares')
const { getProducts, createProduct } = require('../controllers/productsController');

const productRouter = express.Router();

productRouter.get('/', getProducts);

productRouter.post('/', validateProduct, createProduct);

module.exports = productRouter;