const connection = require('./connection');

const getComments = async () => {
  const query = 'SELECT * FROM WebMaisProducts.product_comments;';
  const [result] = await connection.execute(query);

  return result;
};

const create = async ({ comment, productId }) => {
  const query = `INSERT INTO WebMaisProducts.product_comments (comment, product_id) VALUES
  (?, ?);`;

const [result] = await connection.execute(query, [comment, productId]);

return result.insertId;
}

module.exports = {
  create,
  getComments,
}