DROP DATABASE IF EXISTS WebMaisProducts;

CREATE DATABASE WebMaisProducts;

USE WebMaisProducts;

CREATE TABLE product (
    id INT NOT NULL auto_increment,
    code INT NOT NULL,
    name VARCHAR(30) NOT NULL,
    height INT NOT NULL,
    width INT NOT NULL,
    depth INT NOT NULL,
    PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE product_comments (
	id INT NOT NULL AUTO_INCREMENT,
    comment VARCHAR(200) NOT NULL,
    product_id INT NOT NULL,
    FOREIGN KEY (product_id)
        REFERENCES product (id),
	PRIMARY KEY(id)
) ENGINE=INNODB;

INSERT INTO WebMaisProducts.product (code, name, height, width, depth) VALUES
    (2210, "Sofá", 60, 200, 80),
    (3310, "Geladeira", 192, 70, 82),
    (2215, "Poltrona", 78, 65, 90);
    
INSERT INTO WebMaisProducts.product_comments (comment, product_id) VALUES
    ("Ótimo produto!", 2),
    ("Bom, porém após um período dói as costas", 1),
    ("Não gostei da cor :(", 1),
    ("Muito confortável adorei.", 3);
