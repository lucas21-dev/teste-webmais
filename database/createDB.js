const connection = require('../models/connection');

const createSchema = async () => {
  await connection.execute('CREATE DATABASE IF NOT EXISTS WebMaisProducts;');
  console.log('✔️  Banco de Dados "WebMaisProducts" criado\n');

  await connection.execute(`CREATE TABLE IF NOT EXISTS WebMaisProducts.product (
      id INT NOT NULL auto_increment,
      code INT NOT NULL,
      name VARCHAR(30) NOT NULL,
      height INT NOT NULL,
      width INT NOT NULL,
      depth INT NOT NULL,
      PRIMARY KEY(id)
    ) ENGINE=INNODB;`);
  console.log('✔️  Tabela "product" criada');
  await connection.execute(`CREATE TABLE IF NOT EXISTS WebMaisProducts.product_comments (
    id INT NOT NULL AUTO_INCREMENT,
      comment VARCHAR(200) NOT NULL,
      product_id INT NOT NULL,
      FOREIGN KEY (product_id)
          REFERENCES product (id),
    PRIMARY KEY(id)
  ) ENGINE=INNODB;`);
  console.log('✔️  Tabela "product_comments" criada');
};

const seedProduct = async () => {
  await connection.execute(`INSERT INTO WebMaisProducts.product (code, name, height, width, depth) VALUES
  (2210, "Sofá", 60, 200, 80),
  (3310, "Geladeira", 192, 70, 82),
  (2215, "Poltrona", 78, 65, 90);`);
  console.log('✔️  Tabela "products" populada');
};

const seedProductComments = async () => {
  await connection.execute(`INSERT INTO WebMaisProducts.product_comments (comment, product_id) VALUES
  ("Ótimo produto!", 2);`);
  await connection.execute(`INSERT INTO WebMaisProducts.product_comments (comment, product_id) VALUES
  ("Bom, porém após um período dói as costas", 1);`);
  await connection.execute(`INSERT INTO WebMaisProducts.product_comments (comment, product_id) VALUES
  ("Não gostei da cor :(", 1);`);
  await connection.execute(`INSERT INTO WebMaisProducts.product_comments (comment, product_id) VALUES
  ("Muito confortável adorei.", 3);`);
  console.log('✔️  Tabela "product_comments" populada\n');
};

const recreateDatabase = async () => {
  try {
    await createSchema();
    await seedProduct();
    await seedProductComments();
    await connection.end();
    console.log('🥳  Banco de Dados "webMais" populado com sucesso');
  } catch (err) {
    console.log(err);
  }
};

setTimeout(() => {
  recreateDatabase()
}, 18000) ;