const joi = require('joi');

const commentCreate = joi.object({
  code: joi.number().positive().required().strict()
  .messages({
    'any.required': '400|O campo "code" é obrigatório',
    'number.positive': '422|O campo "code" deve ser igual ou maior que 1',
    'number.base': '422|O campo "code" deve ser um número',
  }),
  comment: joi.string().min(1).required().messages({
    'any.required': '400|O campo "comment" é obrigatório',
    'string.min': '422|O campo "comment" deve ter mais de um caractere',
    'string.base': '422|O campo "comment" deve ser uma string'
  }),
  productId: joi.number().positive().required().strict()
    .messages({
      'any.required': '400|O campo "productId" é obrigatório',
      'number.positive': '422|O campo "productId" deve ser igual ou maior que 1',
      'number.base': '422|O campo "productId" deve ser um número'
  }),
});

module.exports = commentCreate;