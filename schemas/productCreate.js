const joi = require('joi');

const productCreate = joi.object({
  name: joi.string().min(1).required().messages({
    'any.required': '400|O campo "name" é obrigatório',
    'string.min': '422|O campo "name" deve ter mais de um caractere',
    'string.base': '422|O campo "name" deve ser uma string'
  }),
  height: joi.number().positive().required().strict()
    .messages({
      'any.required': '400|O campo "height" é obrigatório',
      'number.positive': '422|O campo "height" deve ser igual ou maior que 1',
      'number.base': '422|O campo "height" deve ser um número',
  }),
  width: joi.number().positive().required().strict()
    .messages({
      'any.required': '400|O campo "width" é obrigatório',
      'number.positive': '422|O campo "width" deve ser igual ou maior que 1',
      'number.base': '422|O campo "width" deve ser um número'
  }),
  depth: joi.number().positive().required().strict()
    .messages({
      'any.required': '400|O campo "depth" é obrigatório',
      'number.positive': '422|O campo "depth" deve ser igual ou maior que 1',
      'number.base': '422|O campo "depth" deve ser um número'
  }),
});

module.exports = productCreate;