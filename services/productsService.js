const { getAll, create } = require('../models/ProductsModel');

const createService = async ({ code, name, height, width, depth }) => {
  const products = await getAll();

  const verifyProduct = products.filter((product) => product.code === code);
  if(verifyProduct.length) return null;

  const productId = await create({ code, name, height, width, depth });

  return {
    newProduct: {
      id: productId,
      code,
      name,
      height,
      width,
      depth
    },
  };
}

module.exports = {
  createService,
}