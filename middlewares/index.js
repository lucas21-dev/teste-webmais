const errorHandler = require('./ErrorHandler');
const validateProduct = require('./validateProduct');
const validateComment = require('./validateComment')

module.exports = {
  errorHandler,
  validateProduct,
  validateComment,
};
