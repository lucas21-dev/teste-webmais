const connection = require('./connection');

const getAll = async () => {
  const query = 'SELECT * FROM WebMaisProducts.product ORDER BY id ASC;';
  const [result] = await connection.execute(query);

  return result;
};

const create = async ({ code, name, height, width, depth }) => {
  const query = `INSERT INTO WebMaisProducts.product (code, name, height, width, depth) VALUES
    (?, ?, ?, ?, ?);`;

  const [result] = await connection.execute(query, [code, name, height, width, depth]);

  return result.insertId;
};

module.exports = {
  getAll,
  create,
}