const productRouter = require('./products.routes');
const commentRouter = require('./comments.routes');

module.exports = {
  productRouter,
  commentRouter,
};