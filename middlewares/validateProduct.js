const productCreate = require('../schemas/productCreate');

const validateProduct = async (req, res, next) => {
  const { name, height, width, depth } = req.body;
  const { error } = productCreate.validate({ name, height, width, depth });

  if (error) {
    const [code, message] = error.message.split('|');
    return res.status(Number(code)).json({ message });
  }

  next();
};

module.exports = validateProduct;