const { createService, getService} = require("../services/commentsService");

const createComment = async (req, res) => {
  const { code, comment, productId } = req.body;
  const createdProduct = await createService({ code, comment, productId });

  if(!createdProduct) return res.status(404).json({ message: 'Produto não encontrado!' });

  return res.status(201).json(createdProduct);
}

const getComments = async (req, res) => {
  const comments = await getService();

  return res.status(200).json(comments);
}

module.exports = {
  createComment,
  getComments,
};