# Teste Webmais


## :page_with_curl: Descrição

Projeto feito para a vaga de desenvolvedor web na WebMais serviços, conforme o pedido do teste técnico, que consistia em implementar um sistema onde é possivel adicionar produtos e comentários sobre os produtos, decidi então implementar uma API REST que se comunica com o banco de dados via requisições HTTP, onde é possível sua comunição com uma aplicação front-end e vários tipos de sistemas.

## 🛠️ Tecnologias Utilizadas

* Javascript
* Node.js
* Express.js
* MySQL
* Dotenv
* Express async errors
* [Joi](https://joi.dev/)
* Docker

***

## :computer: Clonando o projeto

Abra seu terminal e digite o seguinte comando:
```
git clone git@gitlab.com:lucas21-dev/teste-webmais.git

cd teste-webmais
```

## :rocket: Rodando a Aplicação

### Para rodar a aplicação é necessário o docker

#### Baixe o [docker](https://www.docker.com) no site oficial e faça sua instalação


Já dentro da pasta da aplicação rode o comando e aguarde a instalação da imagem docker:
```
docker-compose up
```

> Pode ser que na primeira instalação um conflito faça ele não rodar, então é so parar o container e subir novamente com o mesmo comando

Pronto! agora é só abrir os seguinte links no seu navegador:

http://localhost:3000/products

ou

http://localhost:3000/comments

para fazer as requisições GET!

## O banco de dados

<div>
  <img src='./images/banco-SQL.png'/>
</div>

> O banco tem a tabela product que contém todos os produtos cadastrados, e a tabela product_comments que contém os comentários referenciando o produto via ID. A tabela foi feita utilizando os princípios de normalização de tabelas SQL.

## Utilizando a API

#### Para fazer as requisições POST é necessário utilizar uma ferramenta como insomnia/postman

## Para as requisições na Rota /products

### A requisição GET retorna um array contendo todos os Produtos cadastrados no banco de dados, ela será um Json nesse formato:

```
[
    {
        "id": 1,
        "code": 2210,
        "name": "Sofá",
        "height": 60,
        "width": 200,
        "depth": 80
    },
    {
        "id": 2,
        "code": 3310,
        "name": "Geladeira",
        "height": 192,
        "width": 70,
        "depth": 82
    },
    {
        "id": 3,
        "code": 2215,
        "name": "Poltrona",
        "height": 78,
        "width": 65,
        "depth": 90
    }
]
```

### A requisição POST adiciona um novo produto, que deverá ser enviado via body da requisição no seguinte formato:

```
{
    "code": 2523,
    "name": "Cama Box",
    "height": 60,
    "width": 35,
    "depth": 80
}
```

> O que é verificado:

* É verificado se o "code" já existe na aplicação, não permitindo a adição de um produto que já exista, caso aconteça ele retorna a seguinte mensagem:

```
{
    "message": "O código de produto já está cadastrado!"
}
```

* É verificado se todos os campos são digitados, e se eles estão no formato certo, retornando as seguintes mensagens de erro, informando em que campo houve o erro:

```
{
    "message": "O campo \"name\" deve ser uma string"
}
```

```
{
    "message": "O campo \"name\" é obrigatório"
}
```

#### Caso esteja tudo certo, a requisição retorna o produto que foi adicionado no seguinte formato:

```
{
    "newProduct": {
        "id": 4,
        "code": 2523,
        "name": "Cama Box",
        "height": 60,
        "width": 35,
        "depth": 80
    }
}
```

****

## Para as requisições na Rota /comments

### A requisição GET retorna um array contendo todos os produtos cadastrados com todos os seus comentários nesse formato:

```
[
    {
        "productId": 1,
        "name": "Sofá",
        "comments": [
            "Bom, porém após um período dói as costas",
            "Não gostei da cor :("
        ]
    },
    {
        "productId": 2,
        "name": "Geladeira",
        "comments": [
            "Ótimo produto!"
        ]
    },
    {
        "productId": 3,
        "name": "Poltrona",
        "comments": [
            "Muito confortável adorei."
        ]
    }
]
```

* Caso ainda não tenha nenhum comentário cadastrado o produto retorna na chave "comments" a seguinte string:

```
"Ainda não possui comentários"
```

### A requisição POST adiciona um novo comentário (é permitido um produto ter vários comentários), que deverá ser enviado via body da requisição no seguinte formato:

```
{
    "code": 2210,
    "comment": "Na imagem parecia maior",
    "productId": 2
}
```

> O que é verificado:

* É verificado se o produto existe no banco de dados, caso não exista ele retorna a seguinte mensagem de erro:

```
{
    "message": "Produto não encontrado!"
}
```

* É verificado se todos os campos são digitados, e se estão no formato certo, retornando uma mensagem personalizada para cada campo:

```
{
    "message": "O campo \"comment\" é obrigatório"
}
```

```
{
    "message": "O campo \"productId\" deve ser um número"
}
```



